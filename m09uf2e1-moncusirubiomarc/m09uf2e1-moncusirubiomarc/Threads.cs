﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace m09uf2e1_moncusirubiomarc
{
    class Threads
    {
        private string SmallFile = @"../../../../../Readme.md";
        private string BigFile = @"../../../../../Readme.md";
        private int[] _minGuessingNum = new int[3];
        private int[] _maxGuessingNum = new int[3];
        private int[] _guessedNumber = new int[3];
        private bool[] _correct = new bool[3];
        private int _filelines;
        private static EventWaitHandle[] _threadWaitHandle = new EventWaitHandle[]
        {
            new AutoResetEvent(false),
            new AutoResetEvent(false),
            new AutoResetEvent(false)
        };
        private static EventWaitHandle[] _mainWaitHandle = new EventWaitHandle[]
{
            new AutoResetEvent(false),
            new AutoResetEvent(false),
            new AutoResetEvent(false)
};


        public void FileReaderThread()
        {
            FileTreatment ft = new FileTreatment();
            Thread th = new Thread(ft.printFileLength);
            th.Start(SmallFile);
        }

        public void GuessingGame()
        {
            Array.Fill(_minGuessingNum, 0);
            Array.Fill(_maxGuessingNum, 100);
            Array.Fill(_guessedNumber, -1);
            Array.Fill(_correct, false);
            for (int i = 0; i < 3; i++)
            {
                Thread t = new Thread(
                    new ParameterizedThreadStart(ThreadAction)
                );
                t.Start(i);
            }
            FileTreatment ft = new FileTreatment();
            _filelines = ft.getFileLenght(SmallFile);
            Console.WriteLine($"The file has {_filelines} lines");
            Console.WriteLine($"Let's Start the Game!");
            for (int i = 0; i < 3; i++)
            {
                _threadWaitHandle[i].Set();
            }
            while (!Array.TrueForAll(_correct, delegate (bool x) { return x; }))
            {
                MainAction();
            }

        }
        private void MainAction()
        {
            int thread = WaitHandle.WaitAny(_mainWaitHandle);
            var dif = _guessedNumber[thread] - _filelines;
            if (dif > 0)
            {
                Console.WriteLine($"Thread {thread} guessed over the expected number");
                _maxGuessingNum[thread] = _guessedNumber[thread];
            }
            else if (dif < 0)
            {
                Console.WriteLine($"Thread {thread} guessed under the expected number");
                _minGuessingNum[thread] = _guessedNumber[thread];
            }
            else
            {
                _correct[thread] = true;
                Console.WriteLine($"Thread {thread} is correct");
            }

            _threadWaitHandle[thread].Set();

        }
        private void ThreadAction(object data)
        {
            int thread = Convert.ToInt32(data);
            _threadWaitHandle[thread].WaitOne();
            while (!_correct[thread])
            {
                
                GuessingThread(thread);
                WaitHandle.SignalAndWait(_mainWaitHandle[thread], _threadWaitHandle[thread]);                
            }
        }

        private void GuessingThread(int threadValue)
        {
            Random rand = new Random();
            _guessedNumber[threadValue] = rand.Next(_minGuessingNum[threadValue], _maxGuessingNum[threadValue]);
            Console.WriteLine($"Thread {threadValue} guessed: {_guessedNumber[threadValue]}");
        }
    }
}
